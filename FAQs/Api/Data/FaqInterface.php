<?php

namespace BSS\FAQs\Api\Data;

interface FaqInterface
{
    /**
     * Constants for keys of data array.
     */
    const FAQ_ID = 'faq_id';
    const NAME = 'faq_name';
    const QUESTION = 'faq_question';
    const ANSWER = "faq_answer";
    const CREATED = 'faq_create';
    const MODIFIED = 'faq_modified';
    const STATUS = "faq_status";
    const VIEW = "faq_view";
    const LIKE = "faq_like";
    const DISLIKE = "faq_dislike";

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getQuestion();

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion($question);

    /**
     * @return string
     */
    public function getAnswer();

    /**
     * @param string $answer
     * @return $this
     */
    public function setAnswer($answer);
    
    /**
     * @return bool
     */
    public function getStatus();

    /**
     * @param bool $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getCreatedTime();

    /**
     * @return string
     */
    public function getModifiedRime();
    
    /**
     * @return int
     */
    public function getView();
    
    /**
     * @return int
     */
    public function getLike();

    /**
     * @return int
     */
    public function getDislike();
}
