<?php

namespace BSS\FAQs\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface FaqSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \BSS\FAQs\Api\Data\FaqInterface[]
     */
    public function getItems();

    /**
     * @param \BSS\FAQs\Api\Data\FaqInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
