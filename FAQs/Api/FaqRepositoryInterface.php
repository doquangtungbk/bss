<?php

namespace BSS\FAQs\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use BSS\FAQs\Api\Data\FaqInterface;

interface FaqRepositoryInterface
{
    /**
     * @param int $faqId
     * @return \BSS\FAQs\Api\Data\FaqInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($faqId);

    /**
     * @param \BSS\FAQs\Api\Data\FaqInterface $faq
     * @return \BSS\FAQs\Api\Data\FaqInterface
     */
    public function save(FaqInterface $faq);

    /**
     * @param \BSS\FAQs\Api\Data\FaqInterface $faq
     * @return bool true on success
     */
    public function delete(FaqInterface $faq);

    /**
     * @param int $faqId
     * @return bool true on success
     */
    public function deleteById($faqId);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \BSS\FAQs\Api\Data\FaqSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}