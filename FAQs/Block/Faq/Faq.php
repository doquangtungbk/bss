<?php

namespace BSS\FAQs\Block\Faq;

use BSS\FAQs\Model\FaqCategoryFactory;
use BSS\FAQs\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;
use BSS\FAQs\Model\ResourceModel\FaqCustomer\CollectionFactory as FaqCustomerCollectionFactory;

class Faq extends \Magento\Framework\View\Element\Template
{
    /**
     * @var FaqCollectionFactory
     */
    protected $faqCollectionFactory;

    /**
     * @var \BSS\FAQs\Model\FaqCategoryFactory
     */
    protected $faqCategoryFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param FaqCollectionFactory $faqCollectionFactory
     * @param FaqCategoryFactory $faqCategoryFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FaqCollectionFactory $faqCollectionFactory,
        FaqCategoryFactory $faqCategoryFactory,
        FaqCustomerCollectionFactory $faqCustomerCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
		\Magento\Customer\Model\SessionFactory $customerSession
    ) {
        parent::__construct($context);
        $this->faqCollectionFactory = $faqCollectionFactory;
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->customerRepository = $customerRepository;
		$this->customerSession = $customerSession;
        $this->faqCustomerCollectionFactory = $faqCustomerCollectionFactory;
    }

    /**
     * Get FAQ Categories
     *
     * @return array|null
     */
    public function getFaqCategory()
    {
        $faqCategory = $this->faqCategoryFactory->create()->load($this->getData('cate_id'))->getOrigData();
        return $faqCategory;
    }

    /**
     * Get a list of FAQs
     *
     * @return array|null
     */
    public function getActiveFaqs()
    {
        $faqCollection = $this->faqCollectionFactory->create();
        $faqCollection->addFieldToFilter('main_table.cate_id', $this->getData('cate_id'))
                    ->addFieldToFilter('main_table.faq_status', true)->setOrder('faq_sort','ASC');
        return $faqCollection->getData();
    }

    public function getCustomerId()
    {
        $customer = $this->customerSession->create();
		$user = $customer->getCustomer()->getId();
        if ($user != NULL) {
            return $user;
        } else return "NOT_LOGIN";
    }

    public function checkStatus($customer, $faq)
    {
        $faqCustomerResult = $this->faqCustomerCollectionFactory->create();
        $actionCustomer = $faqCustomerResult->addFieldToFilter('main_table.entity_id', $customer)->addFieldToFilter('main_table.faq_id', $faq)->getData();
        $returnAction = 0;
        if (!empty($actionCustomer)){
            $returnAction = $actionCustomer[0]['faq_action'];
        } 
        //var_dump($returnAction);die;
        return $returnAction;
    }
}
