<?php

namespace BSS\FAQs\Block\FaqCategory;

use BSS\FAQs\Model\ResourceModel\FaqCategory\CollectionFactory as FaqCategoryCollectionFactory;

class FaqCategory extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \BSS\FAQs\Model\ResourceModel\FaqCategory\Collection
     */
    protected $faqCategoryCollection;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param FaqCategoryCollectionFactory $faqCategoryCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FaqCategoryCollectionFactory $faqCategoryCollectionFactory
    ) {
        $this->faqCategoryCollection = $faqCategoryCollectionFactory->create();
        parent::__construct($context);
    }

    /**
     * Get a list of FAQ Categories
     *
     * @return array|null
     */
    public function getActiveFaqCategories()
    {
        $this->faqCategoryCollection->addFieldToFilter('main_table.cate_status', true);
        return $this->faqCategoryCollection->getData();
    }
}
