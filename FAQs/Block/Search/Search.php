<?php

namespace BSS\FAQs\Block\Search;

use BSS\FAQs\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;
use BSS\FAQs\Model\ResourceModel\FaqCustomer\CollectionFactory as FaqCustomerCollectionFactory;

class Search extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \BSS\FAQs\Model\ResourceModel\Faq\Collection
     */
    protected $faqCollection;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param FaqCollectionFactory $faqCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FaqCollectionFactory $faqCollectionFactory,
        FaqCustomerCollectionFactory $faqCustomerCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
		\Magento\Customer\Model\SessionFactory $customerSession
    ) {
        parent::__construct($context);
        $this->faqCollection = $faqCollectionFactory->create();
        $this->customerRepository = $customerRepository;
		$this->customerSession = $customerSession;
        $this->faqCustomerCollectionFactory = $faqCustomerCollectionFactory;
    }

    /**
     * Get Text search from url
     *
     * @return string
     */
    public function getTextSearch()
    {
        return ($this->getRequest()->getParam('s')) ? $this->escapeHtml($this->getRequest()->getParam('s')) : '';
    }

    /**
     * Get FAQs via text search
     *
     * @return array|bool
     */
    public function getFaqs()
    {
        $faqCollection = $this->faqCollection->addFieldToFilter('main_table.faq_status', true);
        $textSearch = $this->getTextSearch();
        $faqCollection->addFieldToFilter(['main_table.faq_name', 'main_table.faq_question', 'main_table.faq_answer'],
        [
            ['like' => '%' . $textSearch . '%'],
            ['like' => '%' . $textSearch . '%'],
            ['like' => '%' . $textSearch . '%']
        ]);;
        return $faqCollection->getData();
    }

    public function getCustomerId()
    {
        $customer = $this->customerSession->create();
		$user = $customer->getCustomer()->getId();
        if ($user != NULL) {
            return $user;
        } else return "NOT_LOGIN";
    }

    public function checkStatus($customer, $faq)
    {
        $faqCustomerResult = $this->faqCustomerCollectionFactory->create();
        $actionCustomer = $faqCustomerResult->addFieldToFilter('main_table.entity_id', $customer)->addFieldToFilter('main_table.faq_id', $faq)->getData();
        $returnAction = 0;
        if (!empty($actionCustomer)){
            $returnAction = $actionCustomer[0]['faq_action'];
        } 
        //var_dump($returnAction);die;
        return $returnAction;
    }


}
