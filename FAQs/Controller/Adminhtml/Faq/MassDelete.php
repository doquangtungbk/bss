<?php

namespace BSS\FAQs\Controller\Adminhtml\Faq;

use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\Model\View\Result\RedirectFactory;
use BSS\FAQs\Model\FaqRepositoryFactory;

class MassDelete extends Action
{

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    private $resultRedirect;

    /**
     * @param Action\Context $context
     * @param FaqRepositoryFactory $faqRepositoryFactory,
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context $context,
        FaqRepositoryFactory $faqRepositoryFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->faqRepositoryFactory = $faqRepositoryFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $faqIds = $this->getRequest()->getParams()['selected'];

        $deleted = 0;
        $err = 0;
        foreach ($faqIds as $faqId) {
            $deleteFaq = $this->faqRepositoryFactory->create();
            try {
                $deleteFaq->deleteById($faqId);
                $deleted++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($deleted > 0) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $deleted)
            );
        }

        if ($err > 0) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('faqs/faq/index');
    }
}
