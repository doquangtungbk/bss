<?php

namespace BSS\FAQs\Controller\Adminhtml\Faq;

use BSS\FAQs\Model\FaqFactory;
use Magento\Backend\App\Action;
use Magento\Authorization\Model\UserContextInterface;
use BSS\FAQs\Model\FaqRepositoryFactory;

class Save extends Action
{
    /**
     * @var \BSS\FAQs\Model\FaqFactory
     */
    private $faqFactory;
    
    /**
     * @var \BSS\FAQs\Model\FaqRepositoryFactory
     */
    private $faqRepositoryFactory;

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @param Action\Context $context
     * @param FaqFactory $faqFactory
     */
    public function __construct(
        Action\Context $context,
        FaqFactory $faqFactory,
        FaqRepositoryFactory $faqRepositoryFactory,
        UserContextInterface $userContext,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    ) {
        parent::__construct($context);
        $this->faqFactory = $faqFactory;
        $this->userContext = $userContext;
        $this->faqRepositoryFactory = $faqRepositoryFactory;
        $this->date = $date;
		$this->timezone = $timezone;
    }

    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        //var_dump($postData);die;
        $id = !empty($postData['faq_id']) ? $postData['faq_id'] : null;
        $date = $this->date->gmtDate();
		$date = $this->timezone->date(new \DateTime($date))->format('Y-m-d H:i:s');
        $faq = $this->faqFactory->create();
        $faqRepository = $this->faqRepositoryFactory->create();
        if ($id != null) {
            //$rowEdit = $faq->getById($id)->getData();
            //var_dump($rowEdit);die;
            $faq = $faqRepository->getById($id);
            try
            {
                $faq->setData('cate_id', $postData['cate_id']);
                $faq->setData('faq_name', $postData['faq_name']);
                $faq->setData('faq_question', $postData['faq_question']);
                $faq->setData('faq_answer', $postData['faq_answer']);
                $faq->setData('faq_status', $postData['faq_status']);
                $faq->setData('faq_sort', $postData['faq_sort']);
                $faq->setData('faq_modified', $date);
                $faqRepository->save($faq);
            }
            catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
            }
        }
        else {
            $newData = [
                'user_create' => $this->userContext->getUserId(),
                'cate_id' => $postData['cate_id'],
                'faq_name' => $postData['faq_name'],
                'faq_question' => $postData['faq_question'],
                'faq_answer' => $postData['faq_answer'],
                'faq_status' => $postData['faq_status'],
                'faq_view' => 0,
                'faq_like' => 0,
                'faq_dislike' => 0,
                'faq_create' => $date,
                'faq_modified' => $date,
                'faq_sort' => $postData['faq_sort']
            ];
            try 
            {
                $faq->addData($newData);
                $faqRepository->save($faq);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
            $this->messageManager->addSuccessMessage(__('You saved the FAQ.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['id' => $faq->getId(), '_current' => true]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
