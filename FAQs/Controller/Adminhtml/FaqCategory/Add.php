<?php

namespace BSS\FAQs\Controller\Adminhtml\FaqCategory;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class Add extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New FAQ Category'));
        return $resultPage;
    }
}
