<?php

namespace BSS\FAQs\Controller\Adminhtml\FaqCategory;

use BSS\FAQs\Model\FaqCategoryFactory;
use BSS\FAQs\Model\ResourceModel\FaqCategory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;

class MassDelete extends Action
{
    /**
     * @var \BSS\FAQs\Model\FaqCategoryFactory
     */
    private $faqCategoryFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    private $resultRedirect;

    /**
     * @param Action\Context $context
     * @param FaqCategoryFactory $faqCategoryFactory
     * @param FaqCategory faqCategory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context $context,
        FaqCategoryFactory $faqCategoryFactory,
        FaqCategory $faqCategory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->faqCategory = $faqCategory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $categoryIds = $this->getRequest()->getParams()['selected'];

        $deleted = 0;
        $err = 0;
        foreach ($categoryIds as $categoryId) {
            $deleteFaqCategory = $this->faqCategoryFactory->create()->load($categoryId);
            try {
                $this->faqCategory->delete($deleteFaqCategory);
                //$deleteFaqCategory->delete();
                $deleted++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($deleted > 0) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $deleted)
            );
        }

        if ($err > 0) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('faqs/faqcategory/index');
    }
}
