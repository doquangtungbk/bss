<?php

namespace BSS\FAQs\Controller\Adminhtml\FaqCategory;

use BSS\FAQs\Model\FaqCategoryFactory;
use BSS\FAQs\Model\ResourceModel\FaqCategory;
use BSS\FAQs\Model\FaqCategoryRepositoryFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var BSS\FAQs\Model\FaqCategoryFactory
     */
    private $faqCategoryFactory;

    /**
     * @param Action\Context $context
     * @param FaqCategoryFactory $faqCategoryFactory
     */
    public function __construct(
        Action\Context $context,
        FaqCategoryFactory $faqCategoryFactory,
        FaqCategoryRepositoryFactory $faqCategoryRepositoryFactory,
        FaqCategory $faqCategory
    ) {
        parent::__construct($context);
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->faqCategory = $faqCategory;
        $this->faqCategoryRepositoryFactory = $faqCategoryRepositoryFactory;
    }

    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        $id = !empty($postData['category_id']) ? $postData['category_id'] : null;
         
        $newData = [
            'cate_name' => $postData['cate_name'],
            'cate_status' => $postData['cate_status'],
            'cate_description' => $postData['cate_description']
        ];

        $faqCategoryModel = $this->faqCategoryFactory->create();
        $faqCategoryRepository = $this->faqCategoryRepositoryFactory->create();
        if ($id != null) {
            $faqCategoryModel = $faqCategoryRepository->getById($id);
        }
        try {
            $faqCategoryModel->addData($newData);
            $this->faqCategory->save($faqCategoryModel);
            $this->messageManager->addSuccessMessage(__('You saved the FAQ Category.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['id' => $faqCategoryModel->getId(), '_current' => true]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
