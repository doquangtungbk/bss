<?php
 
namespace BSS\FAQs\Controller\Ajax;

use BSS\FAQs\Model\FaqFactory;
use BSS\FAQs\Model\FaqRepositoryFactory;
use BSS\FAQs\Model\ResourceModel\FaqCustomer\CollectionFactory as FaqCustomerCollectionFactory;
use BSS\FAQs\Model\FaqCustomerFactory;
use BSS\FAQs\Model\ResourceModel\FaqCustomer;
class Dislike extends \Magento\Framework\App\Action\Action
{
    protected $json;
    protected const NO_ACTION = 0;
    protected const LIKE = 1;
    protected const DISLIKE = 2;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        FaqFactory $faqFactory,
        FaqRepositoryFactory $faqRepositoryFactory,
        FaqCustomerCollectionFactory $faqCustomerCollectionFactory,
        FaqCustomerFactory $faqCustomerFactory,
        FaqCustomer $faqCustomer,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->faqFactory = $faqFactory;
        $this->faqRepositoryFactory = $faqRepositoryFactory;
        $this->faqCustomerCollectionFactory = $faqCustomerCollectionFactory;
        $this->faqCustomerFactory = $faqCustomerFactory;
        $this->faqCustomer = $faqCustomer;
    }
    
    public function execute()
    {
        $paramRequest = $this->getRequest()->getParams();
        $id = $paramRequest["idFaq"];
        $action = $paramRequest["action"];

        $faq = $this->faqFactory->create();
        $faqRepository = $this->faqRepositoryFactory->create();
        $resultJson = $this->resultJsonFactory->create();
        $chooseFaq = $faqRepository->getById($id);
        $faq = $chooseFaq;
        $customer = $paramRequest["customer"];
        $faqCustomerResult = $this->faqCustomerCollectionFactory->create();
        $actionCustomer = $faqCustomerResult->addFieldToFilter('main_table.entity_id', $customer)->addFieldToFilter('main_table.faq_id', $id)->getData();
        $faqCustomerModel = $this->faqCustomerFactory->create();
        if (!empty($actionCustomer)) {
                $faqCustomerModel->load($actionCustomer[0]['action_id']);
                if ($actionCustomer[0]['faq_action'] == self::LIKE) {
                    $faqCustomerModel->setData('faq_action', 2);
                    try {
                        $faq->setData('faq_like', $chooseFaq['faq_like'] - 1);
                        $faq->setData('faq_dislike', $chooseFaq['faq_dislike'] + 1);
                        $faqRepository->save($faq);
                    } catch (\Exception $e) {
                        $this->messageManager->addErrorMessage(__($e->getMessage()));
                    }
                    $resultJson->setData(['DISLIKE' => $id, 'STATUS' => 'CHANGE']);
                }
                else if ($actionCustomer[0]['faq_action'] == self::NO_ACTION) {
                    $faqCustomerModel->setData('faq_action', 2);
                    try {
                        $faq->setData('faq_dislike', $chooseFaq['faq_dislike'] + 1);
                        $faqRepository->save($faq);
                    } catch (\Exception $e) {
                        $this->messageManager->addErrorMessage(__($e->getMessage()));
                    }
                    $resultJson->setData(['DISLIKE' => $id, 'STATUS' => 'DISLIKE']);
                } else if ($actionCustomer[0]['faq_action'] == self::DISLIKE) {
                    $faqCustomerModel->setData('faq_action', 0);
                    try {
                        $faq->setData('faq_dislike', $chooseFaq['faq_dislike'] - 1);
                        $faqRepository->save($faq);
                    } catch (\Exception $e) {
                        $this->messageManager->addErrorMessage(__($e->getMessage()));
                    }
                    $resultJson->setData(['DISLIKE' => $id, 'STATUS' => 'UNDISLIKE']);
                }
                try {
                    $this->faqCustomer->save($faqCustomerModel);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
            } else {
                $newData = [
                    'entity_id' => $customer,
                    'faq_id' => $id,
                    'faq_action' => 2
                ];
                try {
                    $faqCustomerModel->addData($newData);
                    $this->faqCustomer->save($faqCustomerModel);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
                try {
                    $faq->setData('faq_dislike', $chooseFaq['faq_dislike'] + 1);
                    $faqRepository->save($faq);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
                $resultJson->setData(['DISLIKE' => $id, 'STATUS' => 'DISLIKE']);
            }
        return $resultJson;
    }  
}