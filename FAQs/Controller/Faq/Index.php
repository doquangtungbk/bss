<?php

namespace BSS\FAQs\Controller\Faq;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $categoryId = $this->getRequest()->getParam('id');
        if (!empty($categoryId)) {
            $block = $page->getLayout()->getBlock('faq.faq.view');
            $block->setData('cate_id', $categoryId);
        }
        return $page;
    }
}
