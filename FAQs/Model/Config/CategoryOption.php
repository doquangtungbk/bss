<?php

namespace BSS\FAQs\Model\Config;

class CategoryOption implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \BSS\FAQs\Model\ResourceModel\FaqCategory\CollectionFactory
     */
    protected $collectionFactory;

    protected $options;

    public function __construct(
        \BSS\FAQs\Model\ResourceModel\FaqCategory\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $collection = $this->collectionFactory->create();
            

            $this->options = [['label' => '', 'value' => '']];

            foreach ($collection as $category) {
                $this->options[] = [
                    'label' => __('%1', $category['cate_name']),
                    'value' => $category->getId()
                ];
            }
        }
        return $this->options;
    }
}
