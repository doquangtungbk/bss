<?php

namespace BSS\FAQs\Model;

use Magento\Framework\Model\AbstractModel;
use BSS\FAQs\Api\Data\FaqInterface;

class Faq extends AbstractModel implements FaqInterface
{
    const CACHE_TAG = 'faq_entity';

    protected $_cacheTag = 'faq_entity';

    protected $_eventPrefix = 'faq_entity';

    protected function _construct()
    {
        $this->_init(\BSS\FAQs\Model\ResourceModel\Faq::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::FAQ_ID);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::FAQ_ID, $id);
    }

    /**
     * @return string
     */
    public function getName() 
    {
        return $this->getData(self::NAME);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name) 
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->getData(self::QUESTION);
    }

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion($question)
    {
        return $this->setData(self::QUESTION, $question);
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->getData(self::ANSWER);
    }

    /**
     * @param string $answer
     * @return $this
     */
    public function setAnswer($answer)
    {
        return $this->setData(self::ANSWER, $answer);
    }
    
    /**
     * @return bool
     */
    public function getStatus() 
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setStatus($status) 
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return string
     */
    public function getCreatedTime() 
    {
        return $this->getData(self::CREATED);
    }

    /**
     * @return string
     */
    public function getModifiedRime() 
    {
        return $this->getData(self::MODIFIED);
    }
    
    /**
     * @return int
     */
    public function getView() 
    {
        return $this->getData(self::VIEW);
    }
    
    /**
     * @return int
     */
    public function getLike()
    {
        return $this->getData(self::LIKE);
    }

    /**
     * @return int
     */
    public function getDislike()
    {
        return $this->getData(self::DISLIKE);
    }
}
