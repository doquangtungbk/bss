<?php

namespace BSS\FAQs\Model;

use Magento\Framework\Model\AbstractModel;


class FaqCategory extends AbstractModel 
{
    const CACHE_TAG = 'faq_category';

    protected $_cacheTag = 'faq_category';

    protected $_eventPrefix = 'faq_category';

    protected function _construct()
    {
        $this->_init(\BSS\FAQs\Model\ResourceModel\FaqCategory::class);
    }
}
