<?php

namespace BSS\FAQs\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use BSS\FAQs\Model\ResourceModel\FaqCategory as ResourceFaqCategory;
use BSS\FAQs\Model\ResourceModel\FaqCategory\CollectionFactory as FaqCategoryCollectionFactory;

class FaqCategoryRepository
{
    /**
     * @var ResourceFaqCategory
     */
    protected $resourceFaqCategory;

    /**
     * @var \BSS\FAQs\Model\FaqCategoryFactory
     */
    protected $faqCategoryFactory;

    /**
     * @var FaqCategoryCollectionFactory
     */
    protected $faqCategoryCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    protected $instances = [];

    /**
     * @param ResourceFaqCategory $resourceFaqCategory
     * @param FaqCategoryFactory $faqCategoryFactory
     * @param FaqCategoryCollectionFactory $faqCategoryCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceFaqCategory $resourceFaqCategory,
        FaqCategoryFactory $faqCategoryFactory,
        FaqCategoryCollectionFactory $faqCategoryCollectionFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resourceFaqCategory = $resourceFaqCategory;
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->faqCategoryCollectionFactory = $faqCategoryCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param int $faqId
     * @throws NoSuchEntityException
     */
    public function getById($faqCategoryId)
    {
        if (!isset($this->instances[$faqCategoryId])) {
            $faqCategory = $this->faqCategoryFactory->create();
            $this->resourceFaqCategory->load($faqCategory, $faqCategoryId);
            if (!$faqCategory->getId()) {
                throw new NoSuchEntityException(__('Faq Category with id "%1" does not exist.', $faqId));
            }
            $this->instances[$faqCategoryId] = $faqCategory;
        }

        return $this->instances[$faqCategoryId];
    }
}
