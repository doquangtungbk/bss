<?php

namespace BSS\FAQs\Model;

use Magento\Framework\Model\AbstractModel;


class FaqCustomer extends AbstractModel 
{
    const CACHE_TAG = 'faq_customer';

    protected $_cacheTag = 'faq_customer';

    protected $_eventPrefix = 'faq_customer';

    protected function _construct()
    {
        $this->_init(\BSS\FAQs\Model\ResourceModel\FaqCustomer::class);
    }
}
