<?php

namespace BSS\FAQs\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use BSS\FAQs\Api\Data\FaqInterface;
use BSS\FAQs\Api\Data\FaqSearchResultsInterfaceFactory;
use BSS\FAQs\Api\FaqRepositoryInterface;
use BSS\FAQs\Model\ResourceModel\Faq as ResourceFaq;
use BSS\FAQs\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;

class FaqRepository implements FaqRepositoryInterface
{
    /**
     * @var ResourceFaq
     */
    protected $resourceFaq;

    /**
     * @var \BSS\FAQs\Model\FaqFactory
     */
    protected $faqFactory;

    /**
     * @var FaqCollectionFactory
     */
    protected $faqCollectionFactory;

    /**
     * @var FaqSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var FaqInterface[]
     */
    protected $instances = [];

    /**
     * @param ResourceFaq $resourceFaq
     * @param FaqFactory $faqFactory
     * @param FaqCollectionFactory $faqCollectionFactory
     * @param FaqSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceFaq $resourceFaq,
        FaqFactory $faqFactory,
        FaqCollectionFactory $faqCollectionFactory,
        FaqSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resourceFaq = $resourceFaq;
        $this->faqFactory = $faqFactory;
        $this->faqCollectionFactory = $faqCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param int $faqId
     * @return FaqInterface
     * @throws NoSuchEntityException
     */
    public function getById($faqId)
    {
        if (!isset($this->instances[$faqId])) {
            $faq = $this->faqFactory->create();
            $this->resourceFaq->load($faq, $faqId);
            if (!$faq->getId()) {
                throw new NoSuchEntityException(__('Faq with id "%1" does not exist.', $faqId));
            }
            $this->instances[$faqId] = $faq;
        }

        return $this->instances[$faqId];
    }

    /**
     * @param FaqInterface $faq
     * @return FaqInterface
     * @throws CouldNotSaveException
     */
    public function save(FaqInterface $faq)
    {
        try {
            $this->resourceFaq->save($faq);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        unset($this->instances[$faq->getId()]);
        return $faq;
    }

    /**
     * @param FaqInterface $faq
     * @return bool true on success
     * @throws CouldNotDeleteException
     */
    public function delete(FaqInterface $faq)
    {
        try {
            $faqId = $faq->getId();
            $this->resourceFaq->delete($faq);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        unset($this->instances[$faqId]);
        return true;
    }

    /**
     * @param int $faqId
     * @return bool true on success
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($faqId)
    {
        return $this->delete($this->getById($faqId));
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \BSS\FAQs\Api\Data\FaqSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \BSS\FAQs\Model\ResourceModel\Faq\FaqCollectionFactory $collection */
        $collection = $this->faqCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var \BSS\FAQs\Api\Data\FaqSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
