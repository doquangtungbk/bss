<?php

namespace BSS\FAQs\Model\ResourceModel\Faq;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'faq_id';

    protected function _construct()
    {

        $this->_init(
            \BSS\FAQs\Model\Faq::class,
            \BSS\FAQs\Model\ResourceModel\Faq::class
        );
    }

    public function _initSelect()
    {
        parent::_initSelect();
        return $this->getSelect()
            ->joinLeft(
                ['secondTable' => $this->getTable('faq_category')],
                'main_table.cate_id = secondTable.cate_id',
                ['cate_name' => 'cate_name']
            )
            ->joinLeft(
                ['thirdTable' => $this->getTable('admin_user')],
                'main_table.user_create = thirdTable.user_id',
                ['user_create_name' => 'username']
            );
    }
}
