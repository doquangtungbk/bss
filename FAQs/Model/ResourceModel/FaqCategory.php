<?php

namespace BSS\FAQs\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class FaqCategory extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('faq_category', 'cate_id');
    }
}
