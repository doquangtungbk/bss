<?php

namespace BSS\FAQs\Model\ResourceModel\FaqCategory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'cate_id';

    protected function _construct()
    {
        $this->_init(
            \BSS\FAQs\Model\FaqCategory::class,
            \BSS\FAQs\Model\ResourceModel\FaqCategory::class
        );
    }
}
