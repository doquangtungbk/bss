<?php

namespace BSS\FAQs\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class FaqCustomer extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('faq_customer', 'action_id');
    }
}
