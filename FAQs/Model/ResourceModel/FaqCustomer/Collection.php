<?php

namespace BSS\FAQs\Model\ResourceModel\FaqCustomer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'action_id';

    protected function _construct()
    {
        $this->_init(
            \BSS\FAQs\Model\FaqCustomer::class,
            \BSS\FAQs\Model\ResourceModel\FaqCustomer::class
        );
    }
}
