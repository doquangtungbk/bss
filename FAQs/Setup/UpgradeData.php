<?php

namespace BSS\FAQs\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use BSS\FAQs\Model\FaqCategoryFactory;
use BSS\FAQs\Model\FaqFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var FaqFactory
     */

    protected $faqFactory;
    /**
     * @var FaqCategoryFactory
     */
    protected $faqCategoryFactory;

    public function __construct(
        FaqFactory $faqFactory,
        FaqCategoryFactory $faqCategoryFactory
        
    ) {
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->faqFactory = $faqFactory;
    }
    
    
    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.4', '<')) {

            // add sample category
            $categoryData = [
                [
                    'cate_name' => 'Sales',
                    'cate_description' => 'This is Question & Answer about sale problem',
                    'icon' => 'https://ibb.co/khT1mhm',
                    'cate_status' => true
                ],
                [
                    'cate_name' => 'Product',
                    'cate_description' => 'This is Question & Answer about product problem',
                    'icon' => 'https://ibb.co/b2pNfmD',
                    'cate_status' => true
                ],
                [
                    'cate_name' => 'Customer',
                    'cate_description' => 'This is Question & Answer about customer problem',
                    'icon' => 'https://ibb.co/fHDRd2t',
                    'cate_status' => true
                ]
            ];
            foreach ($categoryData as $data) {
                $faqCategory = $this->faqCategoryFactory->create();
                $faqCategory->addData($data)->save();
            }

            // add sample faq
            $faqData = [
                [
                    'faq_name' => 'Question about sales',
                    'user_create' => 1,
                    'cate_id' => 1,
                    'faq_status' => true,
                    'faq_like' => 0,
                    'faq_dislike' => 0,
                    'faq_view' => 0,
                    'faq_question' => 'Can I use two discount code in a order?',
                    'faq_answer' => 'No, you can use only one discount code in a order.'
                ],
                [
                    'faq_name' => 'Question about product',
                    'user_create' => 1,
                    'cate_id' => 2,
                    'faq_status' => true,
                    'faq_like' => 0,
                    'faq_dislike' => 0,
                    'faq_view' => 0,
                    'faq_question' => 'Can I get the dowlnload link I forget (have bought) ?',
                    'faq_answer' => 'You must login and go to My Downloadable Produc Tab in Your "My Acount" Page.'
                ],
                [
                    'faq_name' => 'Question about Customer',
                    'user_create' => 1,
                    'cate_id' => 3,
                    'faq_status' => true,
                    'faq_like' => 0,
                    'faq_dislike' => 0,
                    'faq_view' => 0,
                    'faq_question' => 'Can I change my password?',
                    'faq_answer' => 'Yes, You can change your password after login'
                ]
            ];
            foreach ($faqData as $data) {
                $faq = $this->faqFactory->create();
                $faq->addData($data)->save();
            }
        }
    }
}
