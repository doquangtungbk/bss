define([
    'jquery',
    "Magento_Ui/js/modal/modal",
    "mage/url"
 ], function ($, modal, urlBuilder) {
    'use strict';
    var statusFaq = -1;
    //var self = this;
    $.widget('mage.faq',{

        _create: function(option,element) {
            console.log(this.options);
            console.log(this.options.FaqId);
            this.modalFunction(this.options.FaqId, this.options.statusAction, this);

        },
        viewFunction: function(id) {
            var url = urlBuilder.build('faqs/ajax/index');
            $("#value").val(id);
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    idFaq: id,
                    action: 'view',
                },
                success: function(response) {
                   var view = document.querySelector("#modal_view_" + response.KQ);
                   view.textContent = parseInt(view.textContent) + parseInt(1);
                   //console.log(test.textContent)
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        },
        likeFunction: function(self) {
            if ($('#customerID').val() == 0) {
                self.modalConfirmFunction();
                return;
            } 
            if (!$('.Like').hasClass('active-like')) {
                $('.Like').addClass('active-like');
            } else {
                $('.Like').removeClass('active-like');
            }
            var url = urlBuilder.build('faqs/ajax/index');
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    idFaq: $('#value').val(),
                    action: 'like',
                    customer: $('#customerID').val()
                },
                success: function(response) {
                   var status = response.STATUS;
                   var like = document.querySelector("#modal_like_" + response.LIKE);
                   var statusAction = document.querySelector("#statusAction" + response.LIKE);
                   if (status == "LIKE") {
                    like.textContent = parseInt(like.textContent) + parseInt(1);
                    statusAction.textContent = "You liked this FAQ";
                    statusFaq = 1;
                    console.log("Status " + statusFaq);
                   } else if (status == "UNLIKE") {
                    like.textContent = parseInt(like.textContent) - parseInt(1);
                    statusFaq = 0;
                    statusAction.textContent = "";
                    console.log("Status " + statusFaq);
                   } else if (status == "CHANGE") {
                    like.textContent = parseInt(like.textContent) + parseInt(1);
                    var dislike = document.querySelector("#modal_dislike_" + response.LIKE);
                    dislike.textContent = parseInt(dislike.textContent) - parseInt(1);
                    statusAction.textContent = "You liked this FAQ";
                    statusFaq = 1;
                    if ($('.DisLike').hasClass('active-like')) {
                        $('.DisLike').removeClass('active-like');
                    }             
                    } 
                   //console.log(test.textContent)
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        },
        dislikeFunction: function(self) {
            if ($('#customerID').val() == 0) {
                self.modalConfirmFunction();
                return;
            } 
            if (!$('.DisLike').hasClass('active-like')) {
                $('.DisLike').addClass('active-like');
            } else {
                $('.DisLike').removeClass('active-like');
            }
            var url = urlBuilder.build('faqs/ajax/index');
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {
                    idFaq: $('#value').val(),
                    action: 'dislike',
                    customer: $('#customerID').val()
                },
                success: function(response) {
                   var status = response.STATUS;
                   var dislike = document.querySelector("#modal_dislike_" + response.DISLIKE);
                   var statusAction = document.querySelector("#statusAction" + response.DISLIKE);
                   if (status == "DISLIKE") {
                    dislike.textContent = parseInt(dislike.textContent) + parseInt(1);
                    statusAction.textContent = "You disliked this FAQ";
                    statusFaq = 2;
                    console.log("Status " + statusFaq);
                   } else if (status == "UNDISLIKE") {
                    dislike.textContent = parseInt(dislike.textContent) - parseInt(1);
                    statusAction.textContent = "";
                    statusFaq = 0;
                    console.log("Status " + statusFaq);
                   } else if (status == "CHANGE") {
                    dislike.textContent = parseInt(dislike.textContent) + parseInt(1);
                    var like = document.querySelector("#modal_like_" + response.DISLIKE);
                    like.textContent = parseInt(like.textContent) - parseInt(1);
                    statusAction.textContent = "You disliked this FAQ";
                    statusFaq = 2;
                    console.log("Status " + statusFaq);
                    if ($('.Like').hasClass('active-like')) {
                        $('.Like').removeClass('active-like');
                    } 
                   } 
                   //console.log(test.textContent)
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        },
        modalFunction: function(faq, statusActionFaq, self) {
            var optionsFaq = {
                type: 'popup',
                responsive: true,
                title: 'FAQ',
                buttons: [{
                    text: $.mage.__('Ok'),
                    class: '',
                    click: function () {
                        this.closeModal();
                        console.log("Status when close: " + statusFaq);
                    }
                }, {
                    text: $.mage.__('Like'),
                    class: 'Like',
                    click: function () {
                        self.likeFunction(self);
                    }
                }, {
                    text: $.mage.__('Dislike'),
                    class: 'DisLike',
                    click: function () {
                        self.dislikeFunction(self);
                    }
                }]
            };
            var popup = modal(optionsFaq, $('#modal_content_' +  faq));
            $('#modal_faq_' + faq).click(function() {
                console.log(statusFaq);
                self.viewFunction(faq);
                if (statusFaq == -1) {
                    statusFaq = statusActionFaq;
                    //console.log("PHP: " + statusFaq);
                }
                console.log("Status when open: " + statusFaq);
                if (statusFaq == 1) {
                    var statusActionCheck = document.querySelector('#statusAction' + faq);
                    statusActionCheck.textContent = "You liked this FAQ";
                    $('.Like').addClass('active-like');
    
                } else if (statusFaq == 2) {
                    var statusActionCheck = document.querySelector('#statusAction' + faq);
                    statusActionCheck.textContent = "You disliked this FAQ";
                    $('.DisLike').addClass('active-like');
                }
                //console.log(this.options.FaqId);
                $('#modal_content_' +  faq).modal('openModal');
                   
            });

        },
        modalConfirmFunction: function() {
            var optionsFaq = {
                type: 'popup',
                responsive: true,
                title: 'Confirm',
                buttons: [{
                    text: $.mage.__('No'),
                    class: '',
                    click: function () {
                        this.closeModal();
                    }
                }, {
                    text: $.mage.__('Yes'),
                    class: '',
                    click: function () {
                        window.location.replace("https://magento.test/customer/account/login/");
                    }
                }]
            };
            var popup = modal(optionsFaq, $('#modal_content'));
            $('#modal_content').modal('openModal');

        }
    });
     return $.mage.faq;
 });
